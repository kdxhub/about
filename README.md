---
title: "个人声明"
---

<big>本人代表[@kdxiaoyi](//kdx233.github.io/)/[@kdx233](//github.com/kdx233)/[@kdxhub](//github.com/kdxhub)三个账号声明以下内容，请知悉</big>

* 这三个账号的实际控制人均为同一人
* 但无论哪个账号，均未在`gitcode.com`等站点设立仓库镜像
* 唯二官方仓库位于[Github](//github.com/kdxhub)与[Gitee码云](//gitee.com/kdxiaoyi)

